[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/think-big-io/templates/gitpod-vuejs)

[![Netlify Status](https://api.netlify.com/api/v1/badges/604a31b0-13bc-4c47-a23a-01191bb69738/deploy-status)](https://app.netlify.com/sites/derogatory-travel-certificate-covid19/deploys)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
